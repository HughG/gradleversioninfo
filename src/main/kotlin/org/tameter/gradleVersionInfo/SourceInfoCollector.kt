package org.tameter.gradleVersionInfo

import com.fasterxml.jackson.module.kotlin.readValue
import org.tameter.gradleVersionInfo.artifactory.KeyedRepositoryHandle
import org.tameter.gradleVersionInfo.artifactory.ModuleVersion
import org.tameter.gradleVersionInfo.artifactory.RepositoryData
import org.tameter.gradleVersionInfo.artifactory.FileCache
import org.tameter.gradleVersionInfo.ivy.Dependency
import org.tameter.gradleVersionInfo.ivy.IvyModule
import org.tameter.gradleVersionInfo.sequences.firstMappedOrNull
import org.tameter.gradleVersionInfo.serialisation.kotlinXmlMapper
import org.jfrog.artifactory.client.ArtifactoryClientBuilder
import org.tameter.gradleVersionInfo.locators.ArtifactoryPropertiesSourceInfoLocator
import org.tameter.gradleVersionInfo.locators.HolyGradleDefaultBuildInfoSourceInfoLocator
import org.tameter.gradleVersionInfo.util.Maybe
import org.tameter.gradleVersionInfo.util.None
import org.tameter.gradleVersionInfo.util.Some
import org.tameter.gradleVersionInfo.util.asMaybe
import org.tameter.gradleVersionInfo.util.getLogger
import java.io.File
import java.io.InputStream
import java.util.*

/**
 * Provides a method to walk a collection of repositories to find source information for a given module version and all
 * of its transitive dependencies.
 */
class SourceInfoCollector(
    // Kotlin tip: These constructor arguments don't declare fields because they don't have "val" or "var" in front
    // of them; instead they can be used in field initialisation or in any class-level "init" blocks.

    /**
     * The list of repository names to search through, in an Artifactory server.
     */
    repoNames: Collection<String>,

    /**
     * A path under which to store a cache of downloaded information.
     */
    cacheDir: File
) {
    // Kotlin tip: There's no such thing as "static" in Kotlin.  Either declare things at the top level in a file (and
    // you can make them "private" to that file) or in the "companion object", which is a singleton of an unnamed class,
    // the members of which can be accessed with the same syntax as Java.  The companion object can implement
    // interfaces, which can be handy.
    companion object {
        private val logger = getLogger()
    }

    private val repos = repoNames.map {
        val artifactory = ArtifactoryClientBuilder.create()
            .setUrl("https://artifactory.example.com/artifactory/")
            .build()
        KeyedRepositoryHandle(artifactory, it)
    }

    /**
     * A cache of files we download, and other information fetched from Artifactory, partly to speed up re-runs and
     * also because we might visit the same dependency more than once.
     */
    private val fileCache = FileCache(cacheDir)

    //
    /**
     * A map from module versions to information about the source code for them, if available.  This map is to Maybe<T>
     * instead of T? because we need to record the absence of something, and maps treat null as "not in the map".
     */
    private val sourceInfoMap: MutableMap<ModuleVersion, Maybe<SourceInfo>> = TreeMap()

    /**
     * A collection of strategies for finding source information.
     * TODO: Add one for VyLib?
     */
    private val sourceInfoLocators = listOf(
        ArtifactoryPropertiesSourceInfoLocator(fileCache),
        HolyGradleDefaultBuildInfoSourceInfoLocator(fileCache)
    )

    /**
     * An immutable view of the source information found for each transitively-visited module version, after a call to
     * [collectFrom].
     */
    val sourceInfo: Map<ModuleVersion, Maybe<SourceInfo>> get() = sourceInfoMap

    /**
     * Tries to find source information for the given module version and all of its transitive dependencies, and
     * populates the [sourceInfo] map.
     */
    fun collectFrom(baseVersion: ModuleVersion) {
        // Breadth-first search.

        val seen: MutableSet<ModuleVersion> = mutableSetOf()
        val toVisit: Queue<ModuleVersion> = LinkedList()
        seen.add(baseVersion)
        toVisit.add(baseVersion)

        while (toVisit.isNotEmpty()) {
            val version = toVisit.poll()
            logger.info("Visiting ${version}")
            // Find the repository, if any, which contains the ivy.xml metadata for this module version.
            val path = version.ivyPath
            val searchResult = repos.firstDownloadOrNone(version, ModuleVersion::ivyPath)
            when (searchResult) {
                // TODO: Cache "failed to find?"  But what if we just need to add more repos in future?  Flush option?
                is None -> logger.error("Failed to find '${path}' in any repo.")
                is Some -> {
                    // Find the source information for the module version, in the metadata file's repository.
                    val repo = searchResult.v.repository
                    logger.info("Found '${path}' in '${repo.key}'")
                    sourceInfoMap.getOrPut(version) { getSourceInfo(repo, version) }

                    // Visit further transitive dependencies.  This doesn't currently filter out "private"
                    // dependencies which may be used to build something public, but that might be the right approach.
                    val newDependencies = safelyGetDependencies(searchResult, path)
                        .filterNot { seen.contains(it) }
                    seen.addAll(newDependencies)
                    toVisit.addAll(newDependencies)
                }
            }
        }
    }

    // Kotlin tip: You can write "extension" methods for a type, in which "this" is an instance of the type before the
    // dot in the method definition.  This is just a syntactic shorthand for passing it as an argument inside the
    // brackets, which can be useful for brevity and for method call chaining.

    // Kotlin tip: Most kinds of classes and methods can be defined at top level, or inside a class, or even inside a
    // method or lambda.  Having an extension method foo for type A defined inside class B means you can only do
    // "a.foo()" inside B.

    private fun Iterable<KeyedRepositoryHandle>.firstDownloadOrNone(
        version: ModuleVersion,
        // Kotlin tip: This is a function type; in this case, for a function taking one argument and returning String.
        getFilePath: (ModuleVersion) -> String
    ) : Maybe<RepositoryData<InputStream>> {
        return fileCache.tryGetInputStream(this, version, getFilePath).asMaybe()
    }

    // We use Maybe here to indicate that we didn't find something, instead of just a nullable type. because getting a
    // key from a map when there's no value (which would indicate "we haven't looked yet") also returns null by default.
    private fun getSourceInfo(repository: KeyedRepositoryHandle, version: ModuleVersion): Maybe<SourceInfo> {
        val info = sourceInfoLocators.firstMappedOrNull { it.getSourceInfo(repository, version) }.asMaybe()
        when (info) {
            is None -> logger.warn("No source info for ${version} in ${repository}")
            is Some -> logger.info(info.v.toString())
        }
        return info
    }

    private fun safelyGetDependencies(
        searchResult: Some<RepositoryData<InputStream>>,
        path: String
    ): Collection<ModuleVersion> {
        return try {
            getDependencies(searchResult.v.data)
        } catch (e: Exception) {
            logger.error("Failed to read dependencies for '${path}'", e)
            listOf()
        }
    }

    private fun getDependencies(ivyStream: InputStream): Collection<ModuleVersion> {
        ivyStream.use { stream ->
            val module = kotlinXmlMapper.readValue<IvyModule>(stream)
            // Kotlin tip: "?: return" here means we return early with an empty list, if "module.dependencies" is null.
            // This idiom is used quite a lot in Kotlin.
            val deps = module.dependencies ?: return emptyList()
            // Kotlin tip: Sequences are lazy like Java Streams, so this method chain shouldn't allocate a temporary
            // collection for each step.
            return deps.asSequence()
                .mapNotNull { it.asModuleVersion() }
                .filter { version ->
                    listOf("com.example", "org.other-project").any { version.group.startsWith(it) }
                }
                //.take(5)
                .toList()
        }
    }

    private fun Dependency.asModuleVersion(): ModuleVersion? {
        return try {
            // Kotlin tip: postfix "!!" asserts that a maybe-nullable value isn't null, and throws NPE if it is.
            ModuleVersion(org!!, name!!, rev!!)
        } catch (e: NullPointerException) {
            logger.warn("Incomplete dependency information for ${this}")
            null
        }
    }
}