package org.tameter.gradleVersionInfo.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Helper method for easily adding a class-specific logger to a class's companion object.
 */
inline fun <reified T> T.getLogger(): Logger {
    val kClass = T::class
    return LoggerFactory.getLogger(
        if (kClass.isCompanion) kClass.java.enclosingClass else kClass.java
    )
}
