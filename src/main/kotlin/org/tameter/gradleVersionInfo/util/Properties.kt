package org.tameter.gradleVersionInfo.util

import java.net.URL
import java.util.*

fun Properties.load(url: URL) {
    url.openStream().use { load(it) }
}

fun Properties.getString(key: String) =
    this[key] as? String ?: throw IllegalStateException("Could not get property ${key} as String")