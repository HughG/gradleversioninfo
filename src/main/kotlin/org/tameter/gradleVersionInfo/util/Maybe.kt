package org.tameter.gradleVersionInfo.util

// Kotlin tip: a type constraint "out T" is equivalent to Java "<? extends T>".

/**
 * A type representing the absence ([None]) or presence ([Some]) of a non-nullable type.
 * Similar to JDK Optional.
 */
sealed class Maybe<out T: Any>

/**
 * The unique instance of [Maybe] indicating the absence of a value.
 */
object None: Maybe<Nothing>()

/**
 * A subclass of [Maybe] for holding a non-null value.
 */
class Some<out T: Any>(val v: T): Maybe<T>()

/**
 * Converts a null value to [None] and a non-null value to [Some].
 */
fun <T: Any> T?.asMaybe() = if (this == null) None else Some(
    this
)
