package org.tameter.gradleVersionInfo

import org.tameter.gradleVersionInfo.locators.SourceInfoLocator

/**
 * Te collection of source locations (source url with revision) for a given module version as found in one Artifactory
 * [repository].  For information it also stores a [type] string identifying the [SourceInfoLocator] which found the
 * information.
 */
data class SourceInfo(
    val type: String,
    val repository: String,
    val data: Collection<SourceDatum>
)

/**
 * A single source code location, consisting of a URL and revision.
 */
data class SourceDatum(
    val url: String,
    val revision: String
) : Comparable<SourceDatum> {
    override fun toString(): String = "$url@$revision"
    override fun compareTo(other: SourceDatum) = compareValuesBy(this, other, { it.url }, { it.revision })
}