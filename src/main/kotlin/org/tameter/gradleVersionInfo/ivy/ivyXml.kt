package org.tameter.gradleVersionInfo.ivy

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

/**
 * Represents those parts of an Ivy module which are needed for this app.
 */
data class IvyModule(
    val info: Info,
    val dependencies: List<Dependency>?,
    val publications: List<Artifact>?
)

/**
 * Represents those parts of an Ivy module's "info" section which are needed for this app.
 */
data class Info(
    @JacksonXmlProperty(isAttribute = true) val organisation: String,
    @JacksonXmlProperty(isAttribute = true) val module: String,
    @JacksonXmlProperty(isAttribute = true) val revision: String
)

/**
 * Represents those parts of an Ivy module dependency which are needed for this app.
 */
data class Dependency(
    @JacksonXmlProperty(isAttribute = true) val org: String?,
    @JacksonXmlProperty(isAttribute = true) val name: String?,
    @JacksonXmlProperty(isAttribute = true) val rev: String?,
    val artifacts: List<Artifact>?
)

/**
 * Represents those parts of an Ivy module artifact which are needed for this app.
 */
data class Artifact(
    @JacksonXmlProperty(isAttribute = true) val name: String,
    @JacksonXmlProperty(isAttribute = true) val type: String,
    @JacksonXmlProperty(isAttribute = true) val ext: String
)
