package org.tameter.gradleVersionInfo.artifactory

import org.jfrog.artifactory.client.Artifactory
import org.jfrog.artifactory.client.RepositoryHandle
import org.jfrog.artifactory.client.model.Repository

// Kotlin tip: Every class must have exactly one "primary" constructor which is declared after the class name (or
// implicitly has one with no arguments.  If you want to make it private, annotate it, etc., you can explicitly use the
// constructor keyword; other constructors must use that keyword and must call the primary.

/**
 * A subclass of [RepositoryHandle] which has the repository key pre-supplied, to save requesting it again.
 */
class KeyedRepositoryHandle private constructor(
    val key: String, val repositoryHandle: RepositoryHandle
) : RepositoryHandle by repositoryHandle {
    constructor(artifactory: Artifactory, key: String) : this(key, artifactory.repository(key))

    private val repository: Repository by lazy { repositoryHandle.get() }

    override fun get(): Repository = repository

    override fun toString(): String {
        return "repository '${key}'"
    }
}

/**
 * Holder for an object in association with the repository it came from.
 */
data class RepositoryData<T>(
    val repository: KeyedRepositoryHandle,
    val data: T
)

/**
 * Represents a module version or "coordinate" in Artifactory (Ivy/Maven) terms.
 */
data class ModuleVersion(
    val group: String,
    val name: String,
    val version: String
) : Comparable<ModuleVersion> {
    val ivyPath = "$group/$name/$version/ivy-$version.xml"

    override fun toString(): String = "$group:$name:$version"
    @Suppress("MoveLambdaOutsideParentheses")
    override fun compareTo(other: ModuleVersion) = compareValuesBy(this, other, { it.toString() })
}