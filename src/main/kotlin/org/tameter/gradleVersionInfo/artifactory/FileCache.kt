package org.tameter.gradleVersionInfo.artifactory

import org.tameter.gradleVersionInfo.artifactory.KeyedRepositoryHandle
import org.tameter.gradleVersionInfo.artifactory.ModuleVersion
import org.tameter.gradleVersionInfo.artifactory.RepositoryData
import org.tameter.gradleVersionInfo.sequences.firstMappedOrNull
import org.tameter.gradleVersionInfo.util.getLogger
import org.apache.http.HttpStatus
import org.apache.http.client.HttpResponseException
import org.slf4j.Logger
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.lang.IllegalStateException

import org.jfrog.artifactory.client.model.File as ArtifactoryFile

// Kotlin tip: methods can have default arguments.

/**
 * Manages an on-disk cache (in [cacheDir]) of files or other data (e.g., properties) downloaded from Artifactory.
 */
class FileCache(private val cacheDir: File) {
    companion object {
        private val logger: Logger = getLogger()
    }

    init {
        cacheDir.ensureDirExists()
    }

    /**
     * Returns an [InputStream] for the cached contents of file related to a module [version] in a given [repository].
     * The cached path to the file is constructed with the result of calling [getFilePath] on the [version].  The
     * contents to cache are obtained from Artifactory by calling [getFileContents].
     */
    fun tryGetInputStream(
        repository: KeyedRepositoryHandle,
        version: ModuleVersion,
        getFilePath: (ModuleVersion) -> String,
        getFileContents: (repo: KeyedRepositoryHandle, filePath: String) -> InputStream? = this::populateByDownload
    ): InputStream? {
        return tryGetFile(repository, version, getFilePath, getFileContents)?.inputStream()
    }

    /**
     * Returns a cached [File] related to a module [version] in a given [repository].  The cached path to the file is
     * constructed with the result of calling [getFilePath] on the [version].  The contents to cache are obtained from
     * Artifactory by calling [getFileContents].
     */
    fun tryGetFile(
        repository: KeyedRepositoryHandle,
        version: ModuleVersion,
        getFilePath: (ModuleVersion) -> String,
        getFileContents: (repo: KeyedRepositoryHandle, filePath: String) -> InputStream? = this::populateByDownload
    ): File? {
        return tryGetFile(listOf(repository), version, getFilePath, getFileContents)?.data
    }

    /**
     * Searches through a list of repositories to find the first one for which [getFileContents] returns a non-null
     * stream, caches that using a path constructed with the result of calling [getFilePath] on the [version], and
     * returns a [RepositoryData] object associating the matching repository with an [InputStream] of the cached result.
     */
    fun tryGetInputStream(
        repositories: Iterable<KeyedRepositoryHandle>,
        version: ModuleVersion,
        getFilePath: (ModuleVersion) -> String,
        getFileContents: (repo: KeyedRepositoryHandle, filePath: String) -> InputStream? = this::populateByDownload
    ): RepositoryData<InputStream>? {
        // Kotlin tip: putting "?: return null" after a maybe-null expression just means "return early if it's null".
        val file = tryGetFile(repositories, version, getFilePath, getFileContents) ?: return null
        return RepositoryData(file.repository, file.data.inputStream())
    }

    /**
     * Searches through a list of repositories to find the first one for which [getFileContents] returns a non-null
     * stream, caches that using a path constructed with the result of calling [getFilePath] on the [version], and
     * returns a [RepositoryData] object associating the matching repository with a [File] containing the cached result.
     */
    fun tryGetFile(
        repositories: Iterable<KeyedRepositoryHandle>,
        version: ModuleVersion,
        getFilePath: (ModuleVersion) -> String,
        getFileContents: (repo: KeyedRepositoryHandle, filePath: String) -> InputStream?
    ): RepositoryData<File>? {
        val filePath = getFilePath(version)
        val cachedFile =
            getExistingCachedFile(repositories, version, filePath)
                ?: cacheFile(repositories, version, filePath, getFileContents)
        return if (cachedFile != null && cachedFile.data.exists()) {
            cachedFile
        } else {
            null
        }
    }

    private fun File.ensureDirExists() {
        if (this.exists()) {
            if (!this.isDirectory) {
                throw IllegalStateException(
                    "File exists at cache dir path '${this.absolutePath}' exists but it not a directory")
            }
        } else {
            if (!this.mkdirs()) {
                throw IOException("Failed to make cache dir '${this.absolutePath}'")
            }
        }
    }

    /**
     * Returns a [File] object for the path at which to cache a given [filePath] for a module [version] found in some
     * [repository].  This method does not create or otherwise manipulate the file on disk.
     */
    private fun getCachedFile(repository: KeyedRepositoryHandle, version: ModuleVersion, filePath: String): File {
        val cachedFileDir = File(cacheDir, "${repository.key}/${version.group}/${version.name}/${version.version}")
        return File(cachedFileDir, filePath.replace('/', '_'))
    }

    /**
     * Returns a [RepositoryData] object for the first repository for which a cached version of the [filePath] already
     * exists.
     */
    private fun getExistingCachedFile(
        repositories: Iterable<KeyedRepositoryHandle>,
        version: ModuleVersion,
        filePath: String
    ): RepositoryData<File>? {
        // Kotlin tip: Sequences are lazy like Java Streams, so the "map" call won't actually call getCachedFile for
        // every repo: it'll stop after the first successful one, because we're calling "firstOrNull" at the end of this
        // method chain.
        return repositories.asSequence()
            .map { RepositoryData(it, getCachedFile(it, version, filePath)) }
            .firstOrNull { it.data.exists() }
            // Kotlin tip: "also" evaluates to the value before the dot, but also executes a block and ignores its result.
            ?.also { logger.debug("Got cached file for '${filePath}': '${it}'") }
    }

    /**
     * Finds the first of the [repositories] for which [getFileContents] returns a non-null stream, and caches that at
     * the appropriate path for the module [version].  It also caches the Artifactory-provided SHA1 hash of that file,
     * if available.
     */
    private fun cacheFile(
        repositories: Iterable<KeyedRepositoryHandle>,
        version: ModuleVersion,
        filePath: String,
        getFileContents: (repo: KeyedRepositoryHandle, filePath: String) -> InputStream?
    ): RepositoryData<File>? {
        val repoKeyedCachedFile = repositories.firstMappedOrNull { repo ->
                // Kotlin tip: "?.let {}" is null if the value before the dot is null, otherwise it's the value of the
                // block.
                getFileContents(repo, filePath)?.let { Pair(repo, it) }
            } ?: return null // Kotlin tip: early return if null.

        val repo = repoKeyedCachedFile.first
        val cachedFile = getCachedFile(repo, version, filePath)
        cachedFile.parentFile.ensureDirExists()
        // Kotlin tip: "use" is equivalent to Java's "try-with-resources".
        cachedFile.outputStream().use { cachedFileStream ->
            repoKeyedCachedFile.second.copyTo(cachedFileStream)
        }
        val sha1 = tryGet("Did not find info for '${filePath}' in ${repo}") {
            repo.file(filePath).info<ArtifactoryFile>().checksums.sha1
        }
        if (sha1 != null) {
            val hashFile = File(cachedFile.path + ".sha1")
            hashFile.writeText(sha1)
        }
        return RepositoryData(repo, cachedFile)
    }

    /**
     * Returns the (non-null) result of the [action] unless it throws a 404, in which case this method returns null.
     */
    private fun <T> tryGet(message: String, action: () -> T): T? {
        return try {
            action()
        } catch (e: Exception) {
            // Kotlin tip: the "!is" operator means "is not an instance of".
            if (e !is HttpResponseException || e.statusCode != HttpStatus.SC_NOT_FOUND) {
                logger.info(message + ": ${e.message}")
                if (logger.isDebugEnabled) {
                    logger.debug(message, e)
                }
            }
            null
        }
    }

    /**
     * Default way of populating the cache: download the file from the given repository.
     */
    private fun populateByDownload(repo: KeyedRepositoryHandle, filePath: String): InputStream? {
        logger.debug("Download '${filePath}' from ${repo}'")
        return tryGet("Failed to download '${filePath}' from ${repo}") {
            repo.repositoryHandle.download(filePath).doDownload()
        }
    }
}