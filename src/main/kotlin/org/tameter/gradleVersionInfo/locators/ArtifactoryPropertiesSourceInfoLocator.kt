package org.tameter.gradleVersionInfo.locators

import com.fasterxml.jackson.module.kotlin.readValue
import org.tameter.gradleVersionInfo.SourceDatum
import org.tameter.gradleVersionInfo.SourceInfo
import org.tameter.gradleVersionInfo.artifactory.KeyedRepositoryHandle
import org.tameter.gradleVersionInfo.artifactory.ModuleVersion
import org.tameter.gradleVersionInfo.artifactory.FileCache
import org.tameter.gradleVersionInfo.serialisation.kotlinJsonMapper
import org.tameter.gradleVersionInfo.util.getLogger

private const val vcsUrlPropertyName = "vcs.url"
private const val vcsRevisionPropertyName = "vcs.revision"

class ArtifactoryPropertiesSourceInfoLocator(
    private val fileCache: FileCache
): SourceInfoLocator {
    companion object {
        private val logger = getLogger()
        private const val TYPE = "CustomArtifactoryProperties"
    }

    private val ModuleVersion.artifactoryVcsPropertiesPath get() =
        "$group/$name/$version/artifactory-vcs-properties-$version.json"

    override fun getSourceInfo(repository: KeyedRepositoryHandle, version: ModuleVersion): SourceInfo? {
        // This usage of tryGetFile is unusual in that the name of the file we want to cache to is not the same as the
        // name of the file in Artifactory for which we query the properties.
        val cached = fileCache.tryGetInputStream(repository, version, { it.artifactoryVcsPropertiesPath }) { repo, _ ->
            val properties = repo.file(version.ivyPath).getProperties(
                vcsUrlPropertyName,
                vcsRevisionPropertyName
            )
            logger.debug("Properties for ${version} in ${repo}: ${properties}")
            kotlinJsonMapper.writeValueAsString(properties).byteInputStream()
        }

        val properties = cached?.use {
            kotlinJsonMapper.readValue<Map<String, List<String>>>(it)
        } ?: return null // Kotlin tip: return early if null
        val vcsUrls = properties[vcsUrlPropertyName]
        val vcsRevisions = properties[vcsRevisionPropertyName]
        if (vcsUrls == null || vcsRevisions == null) {
            if (vcsUrls == null && vcsRevisions == null) {
                logger.debug("No source info in Artifactory properties for ${version} in ${repository}")
            } else {
                logger.error("Partial source info in Artifactory properties for ${version} in ${repository}: " +
                        "$vcsUrlPropertyName='${vcsUrls}', $vcsRevisionPropertyName='${vcsRevisions}'")
            }
            return null
        } else {
            val vcsUrl = vcsUrls.singleOrNull()
            val vcsRevision = vcsRevisions.singleOrNull()
            if (vcsUrl == null || vcsRevision == null) {
                logger.error(
                    "Incorrect format for source info in Artifactory properties for ${version} in ${repository}: " +
                            "$vcsUrlPropertyName='${vcsUrls}', $vcsRevisionPropertyName='${vcsRevisions}'"
                )
                return null
            }
            return SourceInfo(
                TYPE,
                repository.key,
                listOf(SourceDatum(vcsUrl, vcsRevision))
            )
        }
    }
}