package org.tameter.gradleVersionInfo.locators

import org.tameter.gradleVersionInfo.SourceDatum
import org.tameter.gradleVersionInfo.SourceInfo
import org.tameter.gradleVersionInfo.artifactory.KeyedRepositoryHandle
import org.tameter.gradleVersionInfo.artifactory.ModuleVersion
import org.tameter.gradleVersionInfo.artifactory.FileCache
import org.tameter.gradleVersionInfo.util.getLogger
import java.io.File
import java.util.stream.Stream
import java.util.zip.ZipEntry
import java.util.zip.ZipFile

/**
 * [SourceInfoLocator] which looks at the contents of a downloaded ZIP file.
 */
abstract class ZipContentSourceInfoLocator(
    private val fileCache: FileCache
): SourceInfoLocator {
    companion object {
        private val logger = getLogger()
    }

    protected abstract val ModuleVersion.sourceInfoZipPath: String

    protected abstract val type: String

    protected class TentativeSourceDatum(var url: String?, var rev: String?) {
        constructor(): this(null, null)
        val isComplete get() = (url != null && rev != null)
        val isPartial get() = (url == null) != (rev == null)
    }

    /**
     * Kotlin DSL-style "context" class, which allows overrides of [tryGetSourceData] to just reference the [text]
     * extension property for a [ZipEntry], and have that use the matching [ZipFile], without having to pass it around
     * as an explicit parameter (which would be equivalent, just more verbose at the call site).
     */
    protected class ZipFileContext(private val zipFile: ZipFile) {
        private fun getZipEntryText(file: ZipFile, entry: ZipEntry): String {
            return file.getInputStream(entry).bufferedReader().use { it.readText() }
        }

        val ZipEntry.text: String
            get() = getZipEntryText(zipFile, this)
    }

    protected abstract fun ZipFileContext.tryGetSourceData(entries: Stream<out ZipEntry>)
            : Collection<TentativeSourceDatum>

    final override fun getSourceInfo(repository: KeyedRepositoryHandle, version: ModuleVersion): SourceInfo? {
        fun File.zipFile() = ZipFile(this)

        val data = fileCache.tryGetFile(repository, version, { it.sourceInfoZipPath })
            // Kotlin tip: Using ".?" here means the expression just evaluates to null if the bit before the dot is null
            // otherwise we carry on evaluating the rest of the expression.
            ?.zipFile().use { zipFile ->
                zipFile?.stream()?.let { ZipFileContext(
                    zipFile
                ).tryGetSourceData(it) }
            }
        // Kotlin tip: Using "when" with no value in "()" after it is just a shorthand for "if ... else if ... else".
        if (data == null) {
            logger.debug("No ${type} for ${version} in ${repository}")
            return null
        }
        val completeData = data.filter { datum ->
            if (datum.isPartial) {
                logger.error("Partial ${type} for ${version} in ${repository}: url='${datum.url}', rev='${datum.rev}'")
            }
            datum.isComplete
        }.map { SourceDatum(it.url!!, it.rev!!) }
        return if (completeData.isEmpty()) {
            null
        } else {
            SourceInfo(type, repository.key, completeData)
        }
    }
}