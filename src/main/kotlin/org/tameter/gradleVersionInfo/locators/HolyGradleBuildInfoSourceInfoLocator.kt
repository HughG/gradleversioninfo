package org.tameter.gradleVersionInfo.locators

import org.tameter.gradleVersionInfo.artifactory.ModuleVersion
import org.tameter.gradleVersionInfo.artifactory.FileCache
import java.util.stream.Stream
import java.util.zip.ZipEntry

private const val sourceZipEntryNamePrefix = "build_info/(source_dependencies/([^/]+)/)?"
private const val sourceUrlZipEntryNameSuffix = "source_url.txt"
private const val sourceRevisionZipEntryNameSuffix = "source_revision.txt"
private val sourceInfoZipEntryName =
    "$sourceZipEntryNamePrefix(${Regex.escape(sourceUrlZipEntryNameSuffix)}|${Regex.escape(
        sourceRevisionZipEntryNameSuffix
    )})"
        .toRegex()

abstract class HolyGradleBuildInfoSourceInfoLocator(
    fileCache: FileCache
): ZipContentSourceInfoLocator(fileCache) {
    companion object {
        private const val TYPE = "HolyGradleBuildInfo"
    }

    override val type: String get() = TYPE

    final override fun ZipFileContext.tryGetSourceData(entries: Stream<out ZipEntry>): Collection<TentativeSourceDatum> {
        val data = mutableMapOf<String, TentativeSourceDatum>()

        entries.forEach {
            val result = sourceInfoZipEntryName.matchEntire(it.name)
            if (result != null) {
                val (_/*source dependencies part*/, sourceDependency, filename) = result.destructured
                val datum = data.getOrPut(sourceDependency) { TentativeSourceDatum() }
                // Kotlin tip: "when" is like "select" in Java, but a bit more powerful.
                when (filename) {
                    sourceUrlZipEntryNameSuffix -> datum.url = it.text
                    sourceRevisionZipEntryNameSuffix -> datum.rev= it.text
                }
            }
        }
        return data.values
    }
}

class HolyGradleDefaultBuildInfoSourceInfoLocator(
    fileCache: FileCache
): HolyGradleBuildInfoSourceInfoLocator(fileCache) {
    override val ModuleVersion.sourceInfoZipPath: String
        get() = "$group/$name/$version/$name-buildScript-$version.zip"
}
