package org.tameter.gradleVersionInfo.locators

import org.tameter.gradleVersionInfo.SourceInfo
import org.tameter.gradleVersionInfo.artifactory.KeyedRepositoryHandle
import org.tameter.gradleVersionInfo.artifactory.ModuleVersion

/**
 * A Strategy which can be used to look for [SourceInfo] for a given module version in an Artifactory repository.
 */
interface SourceInfoLocator {
    /**
     * Returrns the [SourceInfo] for a given module [version] in an Artifactory [repository], if it can be found using
     * this strategy; otherwise null.
     */
    fun getSourceInfo(repository: KeyedRepositoryHandle, version: ModuleVersion) : SourceInfo?
}
