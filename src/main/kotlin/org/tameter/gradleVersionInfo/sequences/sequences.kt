package org.tameter.gradleVersionInfo.sequences

fun <T, R : Any> Iterable<T>.firstMappedOrNull(transform: (T) -> R?): R? =
    asSequence().map(transform).filterNotNull().firstOrNull()