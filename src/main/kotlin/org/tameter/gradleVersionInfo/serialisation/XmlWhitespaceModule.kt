package org.tameter.gradleVersionInfo.serialisation

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.BeanDescription
import com.fasterxml.jackson.databind.BeanProperty
import com.fasterxml.jackson.databind.DeserializationConfig
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
import com.fasterxml.jackson.databind.deser.std.CollectionDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.type.CollectionType
import java.io.IOException

/**
 * A Jackson Databind module which allows XML elements to be deserialised as empty collections in cases where only the
 * wrapper element exists, with start and end tags separated by non-empty whitespace, and where there are no wrapped
 * elements within that.  {@link com.fasterxml.jackson.databind.deser.std.CollectionDeserializer.deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext)}
 * doesn't automatically ignore anything except "no whitespace" because in some configurations it's acceptable to have
 * that structure represent "a single-element collection, where the value of the single element is a string containing
 * just whitespace".
 *
 * This class was converted to Kotlin based on https://stackoverflow.com/a/27856622.  An alternative solution would be
 * to pre-process the incoming XML stream to remove all ignorable whitespace (that is, unimportant whitespace between
 * start/end tags), but I found out how to do this solution before I worked out how to get that pre-processing to work.
 */
class XmlWhitespaceModule : SimpleModule() {
    companion object {
        @JvmStatic
        private val serialVersionUID = 1L
    }

    private class CustomizedCollectionDeserialiser(src: CollectionDeserializer) : CollectionDeserializer(src) {
        companion object {
            @JvmStatic
            private val serialVersionUID = 1L
        }

        @Throws(IOException::class, JsonProcessingException::class)
        override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): Collection<*> {
            return if (jp.currentToken == JsonToken.VALUE_STRING && jp.text.matches("^[\\r\\n\\t ]+$".toRegex())) {
                _valueInstantiator.createUsingDefault(ctxt) as Collection<*>
            } else {
                super.deserialize(jp, ctxt)
            }
        }

        @Throws(JsonMappingException::class)
        override fun createContextual(
            ctxt: DeserializationContext,
            property: BeanProperty
        ): CollectionDeserializer {
            return CustomizedCollectionDeserialiser(
                super.createContextual(ctxt, property)
            )
        }
    }

    override fun setupModule(context: SetupContext) {
        super.setupModule(context)
        context.addBeanDeserializerModifier(object : BeanDeserializerModifier() {
            override fun modifyCollectionDeserializer(
                config: DeserializationConfig?, type: CollectionType?,
                beanDesc: BeanDescription?, deserializer: JsonDeserializer<*>
            ): JsonDeserializer<*> {
                return (deserializer as? CollectionDeserializer)?.let {
                    CustomizedCollectionDeserialiser(
                        it
                    )
                }
                    ?: super.modifyCollectionDeserializer(
                        config, type, beanDesc,
                        deserializer
                    )
            }
        })
    }
}