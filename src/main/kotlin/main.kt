package org.tameter.gradleVersionInfo

import org.tameter.gradleVersionInfo.artifactory.ModuleVersion
import org.tameter.gradleVersionInfo.util.Maybe
import org.tameter.gradleVersionInfo.util.None
import org.tameter.gradleVersionInfo.util.Some
import org.tameter.gradleVersionInfo.util.getString
import org.tameter.gradleVersionInfo.util.load
import org.slf4j.LoggerFactory
import org.tameter.gradleVersionInfo.SourceDatum
import org.tameter.gradleVersionInfo.SourceInfo
import org.tameter.gradleVersionInfo.SourceInfoCollector
import java.io.File
import java.io.PrintWriter
import java.util.Properties

// Kotlin tip: The types of variables come *after* the names, separated by colon.  This design choice was made because
// the name ought to be the thing you mostly care about.

private const val thisToolSourceInfoResourceName = "source-info.properties"
private val logger = LoggerFactory.getLogger("org.tameter.gradleVersionInfo.main")

data class Args(val base: ModuleVersion)
class ArgumentValidationException(s: String): IllegalArgumentException(s)

fun main(args: Array<String>) {
    try {
        wrappedMain(args)
    } catch (e: ArgumentValidationException) {
        logger.error(e.message)
    } catch (e: Exception) {
        logger.error("Unhandled exception", e)
    }
}

private fun wrappedMain(rawArgs: Array<String>) {
    // Get this object early so the program can fail if the info isn't available.
    val thisToolSourceInfoMap = getThisToolSourceInfoMap()

    val args = parseArgs(rawArgs)

    // Kotlin tip: the type of variables can be left unstated and will be inferred from the initialisation expression.
    // The return type of methods isn't inferred, though: if you leave it off you get "Unit" which is like "void" in
    // Java.
    val collector = SourceInfoCollector(getRepoNames(), File("downloadCache"))
    collector.collectFrom(args.base)

    val outputFileName = args.base.toString().replace(':', '_') + "-sourceInfo.tsv"
    // Kotlin tip: If the last argument to a method is a lambda, you can put it outside the brackets, to make it look
    // like a control block.  This idiom is used a *lot* in Kotlin.
    File(outputFileName).printWriter().use {
        it.printSourceInfo(thisToolSourceInfoMap)
        it.printSourceInfo(collector.sourceInfo)
    }
}

private fun parseArgs(args: Array<String>): Args {
    val base = try {
        val (group, name, version) = args[0].split(":")
        ModuleVersion(group, name, version)
    } catch (e: Exception) {
        throw ArgumentValidationException(
            "First argument must be a base module version of the format 'group:name:version'")
    }

    return Args(base)
}

private fun getRepoNames(): List<String> {
    // TODO: Maven version of some repos?

    fun just(repo: String) = listOf(repo)

    fun both(repoPrefix: String) = listOf("${repoPrefix}-release", "${repoPrefix}-integration")

    return listOf(
        both("example"),
        just("foo-release")
    ).flatten()
}

/**
 * This intentionally uses a nonsense module coordinate "::" because this app isn't currently published anywhere, so it
 * doesn't _have_ a group:name:version.
 */
private fun getThisToolSourceInfoMap(): Map<ModuleVersion, Some<SourceInfo>> {
    val thisToolSourceInfoFile = Thread.currentThread().contextClassLoader.getResource(thisToolSourceInfoResourceName)
        ?: throw IllegalStateException(
            "Can't read ${thisToolSourceInfoResourceName} to record source info for this program. " +
                    "This file should be part of the program, so this indicates a build error.")

    val thisSourceInfo = Properties().also { it.load(thisToolSourceInfoFile) }
    val sourceUrl = thisSourceInfo.getString("source-remote-url")
    val sourceRevision = thisSourceInfo.getString("source-id")
    val thisSourceDatum = SourceDatum(sourceUrl, sourceRevision)
    return mapOf(
        ModuleVersion("", "", "") to
                Some(
                    SourceInfo(
                        "ThisToolVersion",
                        "",
                        listOf(thisSourceDatum)
                    )
                )
    )}

private fun PrintWriter.printSourceInfo(sourceInfo: Map<ModuleVersion, Maybe<SourceInfo>>) {
    sourceInfo.entries.forEach { entry ->
        val info = entry.value
        val version = when (info) {
            is None -> "not_found"
            is Some<SourceInfo> -> {
                val data = info.v.data
                data.joinToString { "${it.url}@${it.revision}" }
            }
        }
        println("${entry.key}\t${version}")
    }
}
