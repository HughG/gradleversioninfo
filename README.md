# Gradle Source Version Information Collector

This Kotlin program outputs the URL and revision of the source from which all transitive dependencies of a given module
were built.

## How to Use

* Build the project in IntelliJ IDEA (or with Gradle).  If you don't do this first, you can't just run the Run
Configuration because IntelliJ spots that the entry-point class doesn't exist yet.  It would be nicer if it would
just build it first for you, but it doesn't.
* Edit the arguments in the provided IntelliJ IDEA Run Configuration to provide the module and version of interest.
* Run it.
* Keep the generated `<group>_<name>_<version>-sourceInfo.tsv` file.

## Details

The tool traces dependencies reachable through all configurations of each module, so that information is collected about
build and test support modules.  It does this by direct parsing of `ivy.xml` files, rather than by using Gradle's own
dependency resolution mechanisms, because Gradle always wants to download at least one file from each configuration of
each module before you get access to the module metadata, and some of the modules we're interested in only contain very
large files.

The downloaded source version metadata, or a record of its absence, is recorded in the (git-ignored) `downloadCache`
folder, to speed up re-runs of the program (mainly for development convenience).

The output is a TAB-separated value file (`.tsv`) where the first value is the module `group:name:version` coordiante
and the second is a comma-separated list of `<repo_url>@<revision>` strings.  The version info for this program itself
is included with the coordinate `::`. 

## Limitations

### Module selection

* To limit the time to run the tool, and the volume of output, dependencies can be limited to only trace through modules
with certain group strings, e.g., "com.example", "org.otherproj". 

### Dependency Information 
  
* The module dependency metadata must be in `ivy.xml` format.
* Because the `ivy.xml` files are parsed manually, advanced features are not supported, such as dependencies expressed
as version range constraints.

### Source Information

Source information is only collected from the following sources for a module, and is taken from whichever one first
returns _any_ information.  (Note that the list and order here might not match that in the code.)

* The Holy Gradle `build_info` multi-file format, by default only looking in `<name>-buildScript-<version>.zip`.

Other implementations could be added, e.g., to find information from Artifactory properties, or in custom elements in
the `ivy.xml` file.

## Meta

The code has comments to explain Kotlin features and conventions, which should be of help to Java developers.  I chose
Kotlin because it's much more concise than Java, more type-safe than Groovy, and still fairly easy for Java developers
to pick up.
